<footer class="main-footer">
    <strong>Copyright © 2020-2023 <a href="https://adminlte.io">Condheart Inc</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.1.0
    </div>
  </footer>
