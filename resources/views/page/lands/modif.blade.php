@extends('layouts.main')

@section('content')
<div class="col-md-8">
<div class="card card-primary">

    <div class="card-header">
      <h3 class="card-title">Modifiez des lands</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
         @endif

    <form action="{{ route('lands.store')}}" method="POST">
        @csrf
        @method('POST')
      <div class="card-body">
        <div class="form-group">
          <label for="exampleInputEmail1">Libelle</label>
          <input type="text" class="form-control"  placeholder="Entrez le libelle" name="libelle" required value="{{ $lands->libelle}}">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Description</label>
          <input type="text" class="form-control"  placeholder="description" name="description" required value="{{ $lands->description}}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Code</label>
            <input type="text" class="form-control"  placeholder="Entrez le code" name="code_indicatif" required  value="{{ $lands->code_indicatif}}">
          </div>
          <div class="form-group" data-select2-id="29">
            <label>Continent</label>
            <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" required name="continent" value="{{ $lands->continent}}">

              <option value="AAmérique du Nord">Amérique du Nord</option>
              <option value="Amérique du Sud">Amérique du Sud</option>
              <option value="Europe">Europe</option>
              <option value="Océanie">Océanie</option>
              <option value="Antarctique">Antarctique</option>
              <option value="Asie">Asie</option>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Population</label>
            <input type="text" class="form-control"  placeholder="Entrez la population" name="population" required value="{{ $lands->population}}">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Capital</label>
            <input type="text" class="form-control"  placeholder="Entrez le capital" name="capital" required value="{{ $lands->capital}}">
          </div>

          <div class="form-group" data-select2-id="29">
            <label for="exampleInputPassword1">Monnaie</label>
            <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" required name="monnaie" value="{{ $lands->monnaie}}">

              <option value="XOR">XOR</option>
              <option value="EURO">EURO</option>
              <option value="DOLLAR">DOLLAR</option>
            </select>
          </div>
          <div class="form-group" data-select2-id="29">
            <label for="exampleInputPassword1">Langue</label>
            <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" required name="lang" value="{{ $lands->lang}}">

              <option value="FR">FR</option>
              <option value="AR">AR</option>
              <option value="EN">EN</option>
              <option value="ES">ES</option>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Seperficie</label>
            <input type="text" class="form-control"  placeholder="Entrez l superficie" required name="superficie" value="{{ $lands->superficie}}">
          </div>

          <div class="form-group" data-select2-id="29">
            <label for="exampleInputPassword1">Liaque</label>
            <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" required name="est_liaque" value="{{ $lands->est_liaque}}">
              <option value="1">OUI</option>
              <option value="0">NON</option>

            </select>
          </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Enregistrer</button>
      </div>
    </form>
</div>
  </div>



@endsection
