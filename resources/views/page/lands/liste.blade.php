@extends('layouts.main')


@section('content')

<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h1 class="card-title">LA LISTE DES PAYS</h1>

          <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default">
                  <i class="fas fa-search"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table table-hover text-nowrap">
            <thead>
              <tr>
                <th>ID</th>
                <th>Libelle</th>
                <th>Descritpion</th>
                <th>Code</th>
                <th>Continent</th>
                <th>Population</th>
                <th>Capital</th>
                <th>Monnaie</th>
                <th>Langue</th>
                <th>Sperrficie</th>
                <th>Laique</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
                @foreach($lands as $land )
                <tr>
                    <td>{{ $land->id}}</td>
                    <td>{{ $land->libelle}}</td>
                    <td>{{ $land->description}}</td>
                    <td>{{ $land->code_indicatif}}</td>
                    <td>{{ $land->continent}}</td>
                    <td>{{ $land->population}}</td>
                    <td>{{ $land->capital}}</td>
                    <td>{{ $land->cmonnaie}}</td>
                    <td>{{ $land->lang}}</td>
                    <td>{{ $land->superficie}}</td>
                    <td>{{ $land->est_liaque}}</td>
                    <td>
                        <a href="{{route("lands.modif",["id" => $land->id])}}"><button type="button" class="btn btn-block bg-gradient-primary btn-sm">Modifier</button></a>
                    </td>
                    <td>
                        <a href="{{route("lands.delete",["id" => $land->id])}}"><button type="button" class="btn btn-block bg-gradient-danger btn-sm">Supprimer</button></a>
                    </td>

                </tr>

                @endforeach
            </tbody>
          </table>
          <hr>

        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>

@endsection
