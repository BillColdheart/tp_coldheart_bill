@extends('layouts.main')

@section('content')
<div class="col-md-8">
<div class="card card-primary">

    <div class="card-header">
      <h3 class="card-title">AJOUTER UN PAYS</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
         @endif

    <form action="{{ route('lands.store')}}" method="POST">
        @csrf
        @method('POST')
      <div class="card-body">
        <div class="form-group">
          <label for="exampleInputEmail1">Libelle</label>
          <input type="text" class="form-control"  placeholder="Entrez le libelle" name="libelle" required>
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Description</label>
          <input type="text" class="form-control"  placeholder="description" name="description" required>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Code</label>
            <input type="text" class="form-control"  placeholder="Entrez le code" name="code_indicatif" required>
          </div>
          <div class="form-group" data-select2-id="29">
            <label>Continent</label>
            <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" required name="continent">

              <option value="AAmérique du Nord">Afrique</option>
              <option value="Amérique du Sud">Amérique</option>
              <option value="Europe">Europe</option>
              <option value="Océanie">Asie</option>
              <option value="Antarctique">Océanie</option>
              <option value="Asie">Antarctique</option>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Population</label>
            <input type="text" class="form-control"  placeholder="Entrez la population" name="population" required>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Capital</label>
            <input type="text" class="form-control"  placeholder="Entrez le capital" name="capital" required>
          </div>

          <div class="form-group" data-select2-id="29">
            <label for="exampleInputPassword1">Monnaie</label>
            <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" required name="monnaie">

              <option value="XOR">XOR</option>
              <option value="EURO">EURO</option>
              <option value="DOLLAR">DOLLAR</option>
            </select>
          </div>
          <div class="form-group" data-select2-id="29">
            <label for="exampleInputPassword1">Langue</label>
            <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" required name="lang">

              <option value="FR">Français</option>
              <option value="AR">Arab</option>
              <option value="EN">Anglais</option>
              <option value="ES">Espaynol</option>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Seperficie</label>
            <input type="text" class="form-control"  placeholder="Entrez l superficie" required name="superficie">
          </div>

          <div class="form-group" data-select2-id="29">
            <label for="exampleInputPassword1">Liaque</label>
            <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" required name="est_liaque">
              <option value="1">OUI</option>
              <option value="0">NON</option>

            </select>
          </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Enregistrer</button>
      </div>
    </form>
</div>
  </div>



@endsection
