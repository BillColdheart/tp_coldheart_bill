<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LandController;
use App\Models\Land;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts/main');
});


//Route pour afficher la liste des pays dans la table
Route::get('/lands',[LandController::class,'index'])->name('lands.index');

//Route pour ajouter des pays dans la base de données

Route::get('lands/ajout',[LandController::class,'create'])->name(('lands.create'));

//Création d'un land
Route::post('lands/create',[LandController::class,'store'])->name(('lands.store'));

Route::get('lands/ajout',[LandController::class,'create'])->name(('lands.create'));

//Modification d'un land
Route::get('/lands/{id}/edit}',[LandController::class,'edit'])->name(('lands.modif'));

//Suppression d'un land
Route::get('/lands/{id}/delete}',[LandController::class,'destroy'])->name(('lands.delete'));
