<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lands', function (Blueprint $table) {
            $table->id();
            $table->string('libelle')->nullable(false);
            $table->text('description');
            $table->string('code_indicatif')->unique();
            $table->string('continent');
            $table->integer('population');
            $table->string('capital');
            $table->enum('monnaie',['XOF','EURO','DOLLAR']);
            $table->enum('lang',['FR','AR','EN','ES']);
            $table->integer('superficie');
            $table->boolean('est_liaque');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lands');
    }
};
