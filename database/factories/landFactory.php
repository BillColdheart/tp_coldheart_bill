<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\land>
 */
class landFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            //
            'libelle'=>$this->faker->word(),
            'description'=>$this->faker->paragraph(1),
            'code_indicatif'=>$this->faker->numerify("+###"),
            'continent'=>$this->faker->word(),
            'population'=>rand(10000,100000),
            'capital'=>$this->faker->city(),
            'monnaie'=>$this->faker->randomElement(['XOF','EURO','DOLLAR']),
            'lang'=>$this->faker->randomElement(['FR','AR','EN','ES']),
            'superficie'=>rand(1000,52214),
            'est_liaque'=>$this->faker->boolean(),



        ];
    }
}
