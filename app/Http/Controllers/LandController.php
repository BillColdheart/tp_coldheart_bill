<?php

namespace App\Http\Controllers;

use App\Models\Land;
use Illuminate\Http\Request;

class LandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $lands=Land::all();
        return view('page.lands.liste',["lands"=>$lands]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        //
          //insérer nos données dans la bd

         return view('page.lands.ajout');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
            // Validation
        //dd($request);
      $bill =  $request->validate([
            "libelle" => "required|unique:lands,libelle",
            "code_indicatif" => "required",
            "continent" => "required|string",
            "capital" => "required|string",
            "population" => "required|numeric",
            "superficie" => "required|numeric",
            "monnaie" => "required",
            "lang" => "required",
            "est_liaque" => "required"
        ]);

        Land::create($request->all());
        return redirect()->route('lands.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $lands = Land::findOrFail($id);
        return view("page.lands.modif", ["lands" => $lands]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $lands = $request->validate([
            "libelle" => "required|unique:lands,libelle",
            "code_indicatif" => "required",
            "continent" => "required|string",
            "description" => "required",
            "capitale" => "required|string",
            "population" => "required|numeric",
            "superficie" => "required|numeric",
            "monnaie" => "required",
            "langue" => "required",
            "est_liaque" => "required"
        ]);

        // $lands = Land::find($id);

        // $lands->libelle = $request->libelle;
        // $lands->description = $request->description;
        // $lands->code_indicatif = $request->code_indicatif;
        // $lands->continent = $request->continent;
        // $lands->capital = $request->capital;
        // $lands->population = $request->population;
        // $lands->superficie = $request->superficie;
        // $lands->monnaie = $request->monnaie;
        // $lands->lang = $request->lang;
        // $lands->est_liaque = $request->est_liaque;

        // //dd($request);
        // $lands->update();
        Land::whereId($id)->update($lands);

        //$land->save($request->all());
        return redirect()->route('lands.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        land::find($id)->delete();
        return redirect()->route('lands.index');

    }
}
